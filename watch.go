package main

import (
  "os"
  "os/exec"
  "os/signal"
  "log"
  "github.com/fsnotify/fsnotify"
  "syscall"
)

func runBuild (end chan error, quit chan bool) {
  cmd := exec.Command("go", "build")
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr
  err := cmd.Start()
  if err != nil {
    log.Printf("Failed to build")
    return
  }
  cmd.Wait()
  cmd = exec.Command("./" + os.Args[1])
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr
  cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
  err = cmd.Start()
  if err != nil {
    log.Printf("Failed to start server")
    return
  }
  go func () {
    end <- cmd.Wait()
  }()

  select {
    case <-quit:
      if err := syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL); err != nil {
        log.Println("Failed to kill: ", err)
      }
  }
}

func main () {
  watcher, err := fsnotify.NewWatcher()
  if err != nil {
      log.Fatal(err)
  }
  defer watcher.Close()
  done := make(chan bool)
  end := make(chan error, 1)
  quit := make(chan bool)
  c := make(chan os.Signal, 1)
  signal.Notify(c, os.Interrupt, syscall.SIGTERM)

  go func() {
      for {
          select {
          case event := <-watcher.Events:
              if event.Op&fsnotify.Write == fsnotify.Write {
                  log.Println("Modified file:", event.Name)
                  log.Println("Reloading...")
                  quit <- true
              }
          case err := <-watcher.Errors:
              log.Println("error:", err)
          case <-end:
            go runBuild(end, quit)
          case <-c:
            log.Println("Gracefully shutting down")
            quit <- true
            done <- true
            return
          }
      }
  }()
  end <- nil

  err = watcher.Add(".")
  if err != nil {
      log.Fatal(err)
  }
  <-done
  <-end
}
